let covidChart;

async function fetchData() {
    try {
        const countriesResponse = await fetch('https://covid-193.p.rapidapi.com/countries', {
            method: 'GET',
            headers: {
                'X-RapidAPI-Host': 'covid-193.p.rapidapi.com',
                'X-RapidAPI-Key': '52f2181d7bmsh462e5d65a866664p1eb6aajsn06db4dbe2368',
                'Content-Type': 'application/json'
            }
        });
        const countriesData = await countriesResponse.json();

        // Display all countries in the list
        const countryList = document.getElementById('countryList');
        countryList.innerHTML = '';
        countriesData.response.forEach(country => {
            const listItem = document.createElement('div');
            listItem.className = 'country';
            listItem.textContent = country;
            listItem.style.display = 'none';
            countryList.appendChild(listItem);
        });

    } catch (error) {
        console.error('Error fetching data from API:', error);
    }
}

async function fetchCountryData(country) {
    try {
        const response = await fetch(`https://covid-193.p.rapidapi.com/statistics?country=${country}`, {
            method: 'GET',
            headers: {
                'X-RapidAPI-Host': 'covid-193.p.rapidapi.com',
                'X-RapidAPI-Key': '52f2181d7bmsh462e5d65a866664p1eb6aajsn06db4dbe2368',
                'Content-Type': 'application/json'
            }
        });
        const data = await response.json();

        // Check if there is data in the API response
        if (data.response && data.response.length > 0) {
            return data.response[0];
        } else {
            console.error(`Data for the country ${country} is missing.`);
            return null; // Or you can return an empty object or another indicator for missing data
        }
    } catch (error) {
        console.error('Error fetching data for the country from API:', error);
    }
}

async function updateChart(country) {
    const countryData = await fetchCountryData(country);

    if (countryData) {
        const chartCanvas = document.getElementById('covidChart');
        const ctx = chartCanvas.getContext('2d');

        if (covidChart) {
            covidChart.destroy();
        }

        const chartData = {
            labels: ['Cases', 'Deaths', 'Recovered'],
            datasets: [{
                label: `${country} COVID-19 Statistics`,
                data: [
                    countryData.cases.total,
                    countryData.deaths.total,
                    countryData.cases.recovered
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 100, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                ],
                borderWidth: 1
            }]
        };

        const chartOptions = {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        };

        covidChart = new Chart(ctx, {
            type: 'bar',
            data: chartData,
            options: chartOptions
        });

        const infoSection = document.getElementById('chartInfo');
        infoSection.innerHTML = `<p class="info"><span class="label population">Population:</span> <span class="number">${countryData.population.toLocaleString()}</span></p>
                                 <p class="info"><span class="label cases">Cases:</span> <span class="number">${countryData.cases.total.toLocaleString()}</span></p>
                                 <p class="info"><span class="label deaths">Deaths:</span> <span class="number">${countryData.deaths.total.toLocaleString()}</span></p>
                                 <p class="info"><span class="label recovered">Recovered:</span> <span class="number">${countryData.cases.recovered.toLocaleString()}</span></p>`;
        infoSection.style.display = 'block';

    }
}

// Function to add countries and information to the table
async function fetchCountries() {
    try {
        const response = await fetch('https://covid-193.p.rapidapi.com/countries', {
            method: 'GET',
            headers: {
                'X-RapidAPI-Host': 'covid-193.p.rapidapi.com',
                'X-RapidAPI-Key': '52f2181d7bmsh462e5d65a866664p1eb6aajsn06db4dbe2368',
                'Content-Type': 'application/json'
            }
        });

        const data = await response.json();
        updateCountryTable(data.response);
    } catch (error) {
        console.error('Error fetching data from API:', error);
    }
}


async function updateCountryTable(countries) {
    const tableBody = document.getElementById('countryTableBody');
    tableBody.innerHTML = '';

    for (let index = 0; index < countries.length; index++) {
        const country = countries[index];

        try {
            const countryData = await fetchCountryData(country);

            const row = tableBody.insertRow();
            row.insertCell(0).textContent = index + 1;
            row.insertCell(1).textContent = country;
            row.insertCell(2).textContent = countryData?.population ? countryData.population.toLocaleString() : '-';
            row.insertCell(3).textContent = countryData?.cases?.total ? countryData.cases.total.toLocaleString() : '-';
            row.insertCell(4).textContent = countryData?.deaths?.total ? countryData.deaths.total.toLocaleString() : '-';
            row.insertCell(5).textContent = countryData?.cases?.recovered ? countryData.cases.recovered.toLocaleString() : '-';
            row.insertCell(6).textContent = countryData?.continent ? countryData.continent.toLocaleString() : '-';
            row.addEventListener('click', function () {
                updateChart(country);
            });
        } catch (error) {
            console.error(`Error fetching data for ${country}:`, error);
        }
    }
}

// Call fetchCountries to display all countries in the table on page load
document.addEventListener('DOMContentLoaded', fetchCountries);

function filterCountries() {
    const searchInput = document.getElementById('searchCountry').value.toLowerCase();
    const countryList = document.getElementById('countryList');
    const countries = countryList.getElementsByClassName('country');
    const infoSection = document.getElementById('chartInfo');

    if (!searchInput) {
        for (let i = 0; i < countries.length; i++) {
            countries[i].style.display = 'none';
        }
        infoSection.style.display = 'none';
        return;
    }

    for (let i = 0; i < countries.length; i++) {
        const country = countries[i].textContent.toLowerCase();
        if (country.includes(searchInput)) {
            countries[i].style.display = 'block';
            countries[i].addEventListener('click', function () {
                updateChart(country);
            });
        } else {
            countries[i].style.display = 'none';
        }
    }

    if (covidChart) {
        covidChart.destroy();
    }

    infoSection.style.display = 'none';
}



